package edu.boun.edgecloudsim.applications.sample_app5;

import edu.boun.edgecloudsim.utils.SimLogger;
import lombok.Getter;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.*;

public class SumoReader {
    Map<Integer, Vehicle> vehicleMap = new LinkedHashMap<>();
    Map<Integer, Timestep> _timesteps = new HashMap<>();
    Map<Integer, Integer> wlanMap = new HashMap<>(41);

    @Getter
    int vehicleCount;

    @Getter
    int maxId;

    static SumoReader sInstance;

    public static SumoReader getInstance() {
        if(sInstance == null) {
            sInstance = new SumoReader();
        }
        return sInstance;
    }

    public static void reset() {
        sInstance = null;
        System.gc();
    }

    public Vehicle getVehicle(int id) {
        return vehicleMap.get(id);
    }

    public Timestep getSumoStep(int time) {
        return _timesteps.get(time);
    }

    public void checkTimesteps() {
//        for(int i = 0; i < 3600; i += 100) {
//            SimLogger.printLine("time: " + i + _timesteps.get(i).toString());
//        }
    }

    public void parseTimesteps(int numofdevices) {
        SimLogger.print("Parse init for " + numofdevices + " devices");
        _timesteps.clear();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            long time = System.currentTimeMillis();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new File("res/sumoTrace_" + numofdevices + ".xml"));

            doc.getDocumentElement().normalize();

            NodeList timesteps = doc.getDocumentElement().getElementsByTagName("timestep");

            for(int i = 0; i < timesteps.getLength(); i++) {
                Timestep t = parseTimestep(timesteps.item(i));
                _timesteps.put(i,t);
            }

            time = System.currentTimeMillis() - time;

            SimLogger.print("File took " + time + " ms");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    Timestep parseTimestep(Node node) {
        Timestep timestep = new Timestep();
        timestep.time = (new Double(node.getAttributes().getNamedItem("time").getNodeValue())).intValue();

        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node item = list.item(i);
            if(item.getNodeName().equals("vehicle")) {
                timestep.addVehicle(parseVehicle(timestep.time, item));
            }
        }
        return timestep;
    }

    Vehicle parseVehicle(int time, Node node) {
        if (node.getNodeName().equals("vehicle")) {
            NamedNodeMap attributes = node.getAttributes();
            String id = attributes.getNamedItem("id").getTextContent();
            String x = attributes.getNamedItem("x").getTextContent();
            String y = attributes.getNamedItem("y").getTextContent();

            if(id.contains(".")) id = id.substring(id.indexOf(".")+1);

            Integer idx = new Integer(id);
            double dx = Double.parseDouble(x);
            double dy = Double.parseDouble(y);
            int inti = Integer.parseInt(id);

            Vehicle vehicle = new Vehicle(inti,dx > 0 ? dx : 0, dy > 0 ? dy : 0);
            vehicle.arriveTime = time;
            vehicle.departureTime = time;
            vehicle.x = dx > 0 ? dx : 0;
            vehicle.y =  dy > 0 ? dy : 0;
            vehicle.departureX = dx > 0 ? dx : 0;
            vehicle.departureY = dy > 0 ? dy : 0;
            vehicle.arriveX = vehicle.departureX;
            vehicle.arriveY = vehicle.departureY;

            Vehicle commonVehicle = vehicleMap.get(idx);
            if(commonVehicle == null) {
                vehicleMap.put(idx, vehicle);
                vehicleCount++;
                maxId = Math.max(maxId, inti);
            } else {
                commonVehicle.x = dx > 0 ? dx : 0;
                commonVehicle.y = dy > 0 ? dy : 0;
                commonVehicle.arriveX = vehicle.x;
                commonVehicle.arriveY = vehicle.y;
                commonVehicle.arriveTime = time;
            }


            double xmod = vehicle.x/400;
            double xrest = xmod - (int)xmod;
            if(xrest > 0.5) xmod++;

            double ymod = (vehicle.y/25);
            double yrest = ymod - (int)ymod;
            if(yrest > 0.5) ymod++;

            int wlanId = (int)xmod + (int) ymod;
            vehicle.wlanId = wlanId;

            Integer wlanCount = wlanMap.get(wlanId);
            if(wlanCount == null) {
                SimLogger.printLine("First task for id " + wlanId + " from " + vehicle.x + " " + vehicle.y);
                wlanMap.put(wlanId,1);
            } else {
                wlanCount++;
                wlanMap.put(wlanId, wlanCount);
            }

            return vehicle;
        } else {
            SimLogger.printLine("incorrect node tag" + node.getNodeName());
            return null;
        }
    }

    public static class Timestep {
        Map<Integer, Vehicle> vehicles = new HashMap<>();
        int time;

        public void addVehicle(Vehicle v) {
            vehicles.put(v.id, v);
        }

        @Override
        public String toString() {
            return "Timestep{" +
                    "vehicles=" + vehicles +
                    '}';
        }
    }

    public static class Vehicle {
        int id;
        double x;
        double y;
        double departureX;
        double departureY;
        double arriveX;
        double arriveY;
        int departureTime;
        int arriveTime;
        int wlanId;


        public Vehicle(int id, double x, double y) {
            this.id = id;
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "Vehicle{" +
                    "id=" + id +
                    ", x=" + x +
                    ", y=" + y +
                    '}';
        }
    }
}

